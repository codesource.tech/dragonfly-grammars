# Author: Jeremy Hayes
# Modified from: Brandon Lovrien version
# This script includes commands used for HTML coding

from dragonfly import (Grammar, CompoundRule, Dictation, Text, Key, AppContext, MappingRule, Choice)
# import win32com.client
# speaker = win32com.client.Dispatch("SAPI.SpVoice")
import os

class HTMLEnabler(CompoundRule):
    spec = "enable hyper"        # Spoken form of command.

    def _process_recognition(self, node, extras):   # Callback when command is spoken.
        htmlBootstrap.disable()
        htmlGrammar.enable()
        s = "HTML grammar activated"
        print (s)
        # speaker.Speak(s)
        os.system('echo "{0}" | festival --tts'.format(s))

class HTMLDisabler(CompoundRule):
    spec = "switch language"                  # Spoken form of command.

    def _process_recognition(self, node, extras):   # Callback when command is spoken.
        htmlGrammar.disable()
        htmlBootstrap.enable()
        s = "HTML grammar deactivated"
        print (s)
        # speaker.Speak(s)
        os.system('echo "{0}" | festival --tts'.format(s))


class HTMLTestRule(CompoundRule):
    spec = "test HTML"                  # Spoken form of command.

    def _process_recognition(self, node, extras):   # Callback when command is spoken.
        print ("HTML grammar tested")


class HTMLTags(MappingRule):

    mapping  = {
                   "in <tagname> tags": Key("c-x") + Text("<%(tagname)s>") + Key("enter") + Key("c-v")  + Key("enter") + Text("</%(tagname)s>"),
                   "doc type":          Text("<!DOCTYPE HTML>"),
                   "comment":           Text( "<!---->" ) + Key( "left" ) + Key( "left" ) + Key( "left" ),
                   "tags":              Text("<>") + Text("</>"),
                   "<tagname> tags":    Text("<%(tagname)s>") + Text("</%(tagname)s>"),
                   "single tag":        Text("</>"),
                   "line break":        Text( "<br />" ),
                   "image":             Text( "<img />" ),
                   "equals":            Text( "=" ),
                   "<tagname> kick":    Text("</%(tagname)s>") ,#+ Text("</%(tagname)s>"),

                   # used to specify tag attributes
                   "attribute":          Text( ' attributeName=""' ) + Key( "left" ),
                   "<attribute> attribute":                Text( ' %(attribute)s=""' ) + Key( "left" ),

               }
    extras = [
        Choice("attribute", {
                               "ID": "id",
                               "class": "class",
                               "style": "style",
                               "title": "title",
                               "SRC": "src",
                               "HREF": "href",
                               "type": "type",
                               "value": "value",
                               "name": "name",
                               "for": "for",
                               "angular": "ng-",
                               "angular if": "ng-if",
                               "angular show": "ng-show",
                               "angular model": "ng-model",
                               "angular change": "ng-change",
                               "angular click": "ng-click",
                               "angular repeat": "ng-repeat",
                               "angular options": "ng-options",
                               "angular disabled": "ng-disabled",
                               "angular bind": "ng-bind",
                             }
                    ),
        Choice("tagname", {
                              "row": "a-row",
                              "column": "a-col",
                              "card": "a-card",
                              "anchor": "a",
                              "abbreviation": "abbr",
                              "address": "address",
                              "area": "area",
                              "article": "article",
                              "aside": "aside",
                              "audio": "audio",
                              "bold": "b",
                              "base": "base",
                              "BDI": "bdi",
                              "BDO": "bdo",
                              "block quote": "blockquote",
                              "body": "body",
                              "button": "button",
                              "Canvas": "canvas",
                              "table caption": "caption",
                              "cite": "cite",
                              "code": "code",
                              "table column": "col",
                              "table column group": "colgroup",
                              "command": "command",
                              "data list": "datalist",
                              "definition description": "dd",
                              "del": "del",
                              "details": "details",
                              "dfn": "dfn",
                              "divider": "div",
                              "dl": "dl",
                              "dt": "dt",
                              "em": "em",
                              "embed": "embed",
                              "field set": "fieldset",
                              "figure caption": "figcaption",
                              "figure": "figure",
                              "footer": "footer",
                              "form": "form",
                              "header one": "h1",
                              "header to": "h2",
                              "header three": "h3",
                              "header for": "h4",
                              "header five": "h5",
                              "header six": "h6",
                              "head": "head",
                              "header group": "hgroup",
                              "horizontal rule": "hr",
                              "HTML": "html",
                              "italics": "i",
                              "framed": "iframe",
                              "input": "input",
                              "INS": "ins",
                              "key gen": "keygen",
                              "KBD": "kbd",
                              "label": "label",
                              "legend": "legend",
                              "list item": "li",
                              "Link": "link",
                              "Mark": "mark",
                              "menu": "menu",
                              "meta": "meta",
                              "meter": "meter",
                              "nav": "nav",
                              "no script": "noscript",
                              "object": "object",
                              "ordered list": "ol",
                              "option group": "optgroup",
                              "option": "option",
                              "output": "output",
                              "paragraph": "p",
                              "parameter": "param",
                              "pre": "pre",
                              "progress": "progress",
                              "g": "g",
                              "RP": "rp",
                              "RT": "rt",
                              "s": "s",
                              "sample": "samp",
                              "script": "script",
                              "section": "section",
                              "select": "select",
                              "small": "small",
                              "source": "source",
                              "span": "span",
                              "strong": "strong",
                              "style": "style",
                              "sub": "sub",
                              "summary": "summary",
                              "super script": "sup",
                              "table": "table",
                              "table body": "tbody",
                              "table cell": "td",
                              "text area": "textarea",
                              "table foot": "tfoot",
                              "table header": "th", 
                              "table head": "thead",
                              "time": "time",
                              "title": "title",
                              "table row": "tr",
                              "track": "track",
                              "unordered list": "ul",
                              "variable": "var",
                              "video": "video",
                              "label": "label",
                             }
                    )
             ]

#  Code for initial setup of the HTML grammar
htmlBootstrap = Grammar("html bootstrap")                # Create a grammar to contain the command rule.
htmlBootstrap.add_rule(HTMLEnabler())
htmlBootstrap.load()


htmlGrammar = Grammar("html grammar")
htmlGrammar.add_rule(HTMLTestRule())
htmlGrammar.add_rule(HTMLDisabler())
htmlGrammar.add_rule(HTMLTags())
htmlGrammar.load()
htmlGrammar.disable()

# Unload function which will be called by natlink at unload time.
def unload():
    global htmlGrammar
    if htmlGrammar: htmlGrammar.unload()
    htmlGrammar = None
