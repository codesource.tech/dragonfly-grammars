# Author:Jeremy Hayes
# This script includes commands that are to be used for Angular programming

from dragonfly import (Grammar, CompoundRule, Dictation, RuleRef, DictList, DictListRef, Text, Key, AppContext, MappingRule, Function, Sequence, Mimic)
# import win32com.client
# speaker = win32com.client.Dispatch("SAPI.SpVoice")
import os
def doSomethingToCommand(command):
    newCommand = Sequence(command)
    newCommand.execute()

class AngularEnabler(CompoundRule):
    spec = "enable Angular"                  # Spoken form of command.

    def _process_recognition(self, node, extras):   # Callback when command is spoken.
        AngularBootstrap.disable()
        AngularGrammar.enable()
        s = "Angular JS  grammar activated"
        print (s)
        # speaker.Speak(s)
        os.system('echo "{0}" | festival --tts'.format(s))


class AngularDisabler(CompoundRule):
    spec = "switch language"                  # Spoken form of command.

    def _process_recognition(self, node, extras):   # Callback when command is spoken.
        AngularGrammar.disable()
        AngularBootstrap.enable()
        s = "Angular JS  grammar deactivated"
        print (s)
        # speaker.Speak(s)
        os.system('echo "{0}" | festival --tts'.format(s))

class AngularTestRule(CompoundRule):
    spec = "test Angular"                  # Spoken form of command.

    def _process_recognition(self, node, extras):   # Callback when command is spoken.
        print ("Angular JS grammar tested")

class AngularControlStructures(MappingRule):

    mapping  = {
                  "variable":               Text("var "),
                  "function":               Text("function functionName() {") + Key("enter")+ Key("enter"), #+ Text("}"),
                  "self function":          Text("(function() {") + Key("enter") + Key("enter"), #+ Text("}())"),
                  "for each":               Text("angular.forEach( , function(o) {") + Key("enter") + Key("enter") + Text("});") + Key("up"), #+ Key("up"),
                  "timeout":               Text("$timeout(function() {") + Key("enter") + Key("enter") + Text("}, 100);") + Key("up"), #+ Key("up"),
                  "code block":             Text("{") + Key("enter")+ Key("enter"), #+ Text("}"),
                  "if":                     Text("if() {") + Key("enter")+ Key("enter"), #+ Text("}"),
                  "if else":                Text("if() {") + Key("enter")+ Key("enter") + Text("}") + Key("enter") + Text("else {") + Key("enter")+ Key("enter"), #+ Text("}"),
                  "else if":                Text("else if() {") + Key("enter")+ Key("enter"), #+ Text("}"),
                  "while loop":             Text("while() {") + Key("enter")+ Key("enter"), #+ Text("}"),
                  "do while loop":          Text("do {") + Key("enter") + Key("down") + Text("while()"),
                  "for loop":               Text("for(;;) {") + Key("enter")+ Key("enter"), #+ Text("}"),
                  "switch statement":       Text("switch() {") + Key("enter")+ Key("enter"), #+ Text("}"),


               }


class AngularCommentsSyntax(MappingRule):

    mapping  = {
                "comment":                Text("// "),
                "multiline comment":      Text("/*") + Key("enter") #+ Key("enter") + Text("*/") + Key("up")

               }

class AngularMiscellaneousStuff(MappingRule):

    mapping  = {
                   "equals":        Text(" = "),
                   "new":           Text("new "),

               }

class AngularComparisonOperators(MappingRule):

    mapping  = {
                   "equal to":                   Text("=="),
                   "exactly equal to":           Text("==="),
                   "not equal to":               Text("!="),
                   "greater than":               Text(">"),
                   "less than":                  Text("<"),
                   "greater than or equal to":   Text(">="),
                   "less than or equal to":      Text("<="),

               }

class AngularArithmeticOperators(MappingRule):

    mapping  = {
                   "plus plus":                   Text("++"),
                   "minus minus":               Text("--"),

               }

class AngularAssignmentOperators(MappingRule):

    mapping  = {
                   "plus equals":                   Text("+="),
                   "minus equals":                  Text("-="),
                   "multiply equals":               Text("*="),
                   "divide equals":                 Text("/="),
                   "modulus equals":                Text("%="),

               }



AngularBootstrap = Grammar("Angular bootstrap")                # Create a grammar to contain the command rule.
AngularBootstrap.add_rule(AngularEnabler())
AngularBootstrap.load()

AngularGrammar = Grammar("Angular grammar")
AngularGrammar.add_rule(AngularTestRule())
AngularGrammar.add_rule(AngularControlStructures())
AngularGrammar.add_rule(AngularCommentsSyntax())
AngularGrammar.add_rule(AngularMiscellaneousStuff())
AngularGrammar.add_rule(AngularComparisonOperators())
AngularGrammar.add_rule(AngularArithmeticOperators())
AngularGrammar.add_rule(AngularAssignmentOperators())
AngularGrammar.add_rule(AngularDisabler())
AngularGrammar.load()
AngularGrammar.disable()

# Unload function which will be called by natlink at unload time.
def unload():
    global AngularGrammar
    if AngularGrammar: AngularGrammar.unload()
    AngularGrammar = None
