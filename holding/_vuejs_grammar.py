# Author:Jeremy Hayes
# This script includes commands that are to be used for Vue programming

from dragonfly import (Grammar, CompoundRule, Dictation, RuleRef, DictList, DictListRef, Text, Key, AppContext, MappingRule, Function, Sequence, Mimic)
import win32com.client
speaker = win32com.client.Dispatch("SAPI.SpVoice")

def doSomethingToCommand(command):
    newCommand = Sequence(command)
    newCommand.execute()

class VueEnabler(CompoundRule):
    spec = "Activate view"                  # Spoken form of command.

    def _process_recognition(self, node, extras):   # Callback when command is spoken.
        VueBootstrap.disable()
        VueGrammar.enable()
        s = "Vue JS  grammar activated"
        print (s)
        speaker.Speak(s)

class VueDisabler(CompoundRule):
    spec = "switch language"                  # Spoken form of command.

    def _process_recognition(self, node, extras):   # Callback when command is spoken.
        VueGrammar.disable()
        VueBootstrap.enable()
        s = "Vue JS  grammar deactivated"
        print (s)
        speaker.Speak(s)

class VueTestRule(CompoundRule):
    spec = "test View"                  # Spoken form of command.

    def _process_recognition(self, node, extras):   # Callback when command is spoken.
        s = "Vue JS grammar tested"
        print (s)
        speaker.Speak(s)

class VueControlStructures(MappingRule):

    mapping  = {
                  "variable":               Text("var "),
                  "function":               Text("function functionName() {") + Key("enter")+ Key("enter"), #+ Text("}"),
                  "self function":          Text("(function() {") + Key("enter") + Key("enter"), #+ Text("}())"),
                  "code block":             Text("{") + Key("enter")+ Key("enter"), #+ Text("}"),
                  "if":                     Text("if() {") + Key("enter")+ Key("enter"), #+ Text("}"),
                  "if else":                Text("if() {") + Key("enter")+ Key("enter") + Text("}") + Key("enter") + Text("else {") + Key("enter")+ Key("enter"), #+ Text("}"),
                  "else if":                Text("else if() {") + Key("enter")+ Key("enter"), #+ Text("}"),
                  "while loop":             Text("while() {") + Key("enter")+ Key("enter"), #+ Text("}"),
                  "do while loop":          Text("do {") + Key("enter") + Key("down") + Text("while()"),
                  "for loop":               Text("for(;;) {") + Key("enter")+ Key("enter"), #+ Text("}"),
                  "switch statement":       Text("switch() {") + Key("enter")+ Key("enter"), #+ Text("}"),
               }


class VueCommentsSyntax(MappingRule):

    mapping  = {
                "comment":                Text("// "),
                "multiline comment":      Text("/*") + Key("enter") #+ Key("enter") + Text("*/") + Key("up")

               }

class VueMiscellaneousStuff(MappingRule):

    mapping  = {
                   "equals":        Text(" = "),
                   "new":           Text("new "),

               }

class VueComparisonOperators(MappingRule):

    mapping  = {
                   "equal to":                   Text("=="),
                   "exactly equal to":           Text("==="),
                   "not equal to":               Text("!="),
                   "greater than":               Text(">"),
                   "less than":                  Text("<"),
                   "greater than or equal to":   Text(">="),
                   "less than or equal to":      Text("<="),

               }

class VueArithmeticOperators(MappingRule):

    mapping  = {
                   "plus plus":                   Text("++"),
                   "minus minus":               Text("--"),

               }

class VueAssignmentOperators(MappingRule):

    mapping  = {
                   "plus equals":                   Text("+="),
                   "minus equals":                  Text("-="),
                   "multiply equals":               Text("*="),
                   "divide equals":                 Text("/="),
                   "modulus equals":                Text("%="),

               }



VueBootstrap = Grammar("Vue bootstrap")                # Create a grammar to contain the command rule.
VueBootstrap.add_rule(VueEnabler())
VueBootstrap.load()

VueGrammar = Grammar("Vue grammar")
VueGrammar.add_rule(VueTestRule())
VueGrammar.add_rule(VueControlStructures())
VueGrammar.add_rule(VueCommentsSyntax())
VueGrammar.add_rule(VueMiscellaneousStuff())
VueGrammar.add_rule(VueComparisonOperators())
VueGrammar.add_rule(VueArithmeticOperators())
VueGrammar.add_rule(VueAssignmentOperators())
VueGrammar.add_rule(VueDisabler())
VueGrammar.load()
VueGrammar.disable()

# Unload function which will be called by natlink at unload time.
def unload():
    global VueGrammar
    if VueGrammar: VueGrammar.unload()
    VueGrammar = None
