# Author:Jeremy Hayes
# This script is to be used for programming in the Julia programming language

from dragonfly import (Grammar, CompoundRule, Dictation, Text, Key, AppContext, MappingRule)
import win32com.client
speaker = win32com.client.Dispatch("SAPI.SpVoice")

class JuliaEnabler(CompoundRule):
    spec = "Enable Julia"                  # Spoken command to enable the Julia grammar.

    def _process_recognition(self, node, extras):   # Callback when command is spoken.
        JuliaBootstrap.disable()
        JuliaGrammar.enable()
        s = "Julia grammar enabled"
        print (s)
        speaker.Speak(s)

class JuliaDisabler(CompoundRule):
    spec = "switch language"                  # spoken command to disable the Julia grammar.

    def _process_recognition(self, node, extras):   # Callback when command is spoken.
        JuliaGrammar.disable()
        JuliaBootstrap.enable()
        s = "Julia grammar disabled"
        print (s)
        speaker.Speak(s)


# This is a test rule to see if the Julia grammar is enabled
class JuliaTestRule(CompoundRule):
    spec = "test Julia"                  # Spoken form of command.

    def _process_recognition(self, node, extras):   # Callback when command is spoken.
        print ("Julia grammar tested")

# Handles Julia commenting syntax
class JuliaCommentsSyntax(MappingRule):

    mapping  = {
                "comment":                Text("# "),
               }


# handles Julia control structures
class JuliaControlStructures(MappingRule):
    mapping  = {
                    "if":                   Text("if condition:") + Key("enter"),
                    "while loop":           Text("while condition:") + Key("enter"),
                    "for loop":             Text("for something in something:") + Key("enter"),
                    "function":             Text("function functionName()") + Key("enter") + Key("enter") + Text("end"),
                    "class":                Text("class className(inheritance):") + Key("enter"),
               }


# The main Julia grammar rules are activated here
JuliaBootstrap = Grammar("Julia bootstrap")
JuliaBootstrap.add_rule(JuliaEnabler())
JuliaBootstrap.load()

JuliaGrammar = Grammar("Julia grammar")
JuliaGrammar.add_rule(JuliaTestRule())
JuliaGrammar.add_rule(JuliaCommentsSyntax())
JuliaGrammar.add_rule(JuliaControlStructures())
JuliaGrammar.add_rule(JuliaDisabler())
JuliaGrammar.load()
JuliaGrammar.disable()


# Unload function which will be called by natlink at unload time.
def unload():
    global JuliaGrammar
    if JuliaGrammar: JuliaGrammar.unload()
    JuliaGrammar = None
